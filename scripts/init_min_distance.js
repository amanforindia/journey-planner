var async = require('async');

var fs = require('fs');
var db = require('../connectors/cache_db.js');
var cql_statements = require('../util/cql_statements.js');
var trim = require('trim');
var csv_loader = require('fast-csv');

// Private shared variable
var db_connection = db.getInstance();
var cql_queries = cql_statements.SHORTEST_DISTANCE;

var _BATCH_SIZE = 1000;

var InitMinDistance = function() {};

InitMinDistance.prototype.asyncTasks = [];

InitMinDistance.prototype.insertMinDistanceBatch = function(queries, callback) {
  db_connection.batch(queries, { prepare: true }, function(err) {
    if (err) {
      console.log('Error happened: ' + err);
    } else {
      console.log('All data inserted successfully.');
    }
    callback(err);
  })
};

InitMinDistance.prototype.lockAndLoad = function(file_path) {
  var self = this;
  var queries = [];
  var stream = fs.createReadStream(file_path);
  var csv_stream = csv_loader({headers: true}).on('data', function(data) {
    var param_array = [trim(data.station_a.toUpperCase()), trim(data.station_b.toUpperCase())];
    param_array.sort();
    param_array.push(parseInt(data.min_distance));
    queries.push({
      query: cql_queries.INSERT,
      params: param_array
    });
    if (queries.length > _BATCH_SIZE) {
        var queryClone = queries.slice(0, queries.length);
        var asyncFunc = function (callback) {
            this.insertMinDistanceBatch(queryClone, function () {
              callback();
            });
        }.bind(self);
        self.asyncTasks.push(asyncFunc);
        queries = [];
    }
  }).on('end', function() {
      async.series(self.asyncTasks, function(err) {
          if (err) {
              console.log('Error executing cassandra insertions');
          } else {
             console.log('All finished successfully.');
          }
      });
  });

  stream.pipe(csv_stream);
};

module.exports = InitMinDistance;

var minDist = new InitMinDistance();
minDist.lockAndLoad('../data/shortest_routes_infy_half_500.csv');
