/**
 * Created by aman on 20/3/16.
 */
var request = require('request');
var cheerio = require('cheerio');
var async = require('async');
var fs = require('fs');

var start_base = 4000;
var OUT_FILE_PATH = 'data/shortest_routes_infy_half_' + start_base + '.csv';

Array.prototype.diff = function(a) {
  return this.filter(function(i) {return a.indexOf(i) < 0;});
};
var codes = fs.readFileSync('data/all_station_list.csv').toString().split("\n").map(function(x) {
  return x.trim();
}).filter(function(x) { return x !== '';});

var i=start_base;
var j = i+1;
if(!fs.existsSync(OUT_FILE_PATH)) {
  fs.closeSync(fs.openSync(OUT_FILE_PATH, 'w'));
}
var out = fs.readFileSync(OUT_FILE_PATH).toString().split("\n").map(function(x) {
  return x.trim();
}).filter(function(x) { return x !== '';});
if (out.length > 0) {
  i = parseInt(out[out.length - 1].split(',')[3]);
  j = parseInt(out[out.length - 1].split(',')[4]);
  j++;
  if (j == codes.length) {
    i++;
    j = i+1;
  }
  console.log("Resuming from " + codes[i] + " " + codes[j]);
  console.log("Resume indices i:" + i + " j: " + j);
}
out = null;

var allRoutes = [];
var count = 0;
for(;i<start_base+500 && i<codes.length - 1;) {
  var routes = {srcCode: codes[i], route: 'uts'};
  var dests = [];
  var jstart = j;
  var istart = i;
  for(;j<codes.length && count < 22000;j++,count++) {
    routes['uts' + (count + 1)] = codes[i] + ',' + codes[j];
    dests.push(codes[j]);
  }
  if (j == codes.length) {
    i++;
    j = i+1;
    count = 0;
  }
  if (count == 22000) {
    count = 0;
  }
  allRoutes.push({form: routes, src: routes.srcCode, dests: dests, i: istart, j: jstart});
}
var logCount = 1;
async.mapSeries(allRoutes, function(routes, cb) {
  var repeat_infinite = function() {
    console.log(new Date().toString() + " Req start");
    request.post({url:'http://rbs.indianrail.gov.in/ShortPath/UtsRouteServlet',
      form: routes.form}, function(err, res, body) {
      console.log(new Date().toString() + " Req end");
      var distances = [], dests = [];
      if(err) {
        console.log(err);
        console.log(routes.src);
        console.log(routes.dests.join(','));
        console.log(routes.i);
        console.log(routes.j);
        setTimeout(repeat_infinite, 1000);
        return;
      }
      var cheerioBody = cheerio.load(body);
      var cheerioDists = cheerioBody('table#rtTable tr > td:nth-child(4)');
      var cheerioStarts = cheerioBody('table#rtTable tr > td:nth-child(2)');
      var cheerioEnds = cheerioBody('table#rtTable tr > td:nth-child(3)');
      for(var k=1;k<= routes.dests.length; k++) {
        var dist = cheerioDists.eq(k).text();
        var start = cheerioStarts.eq(k).text();
        var end = cheerioEnds.eq(k).text();
        if(dist !== '') {
          distances.push(start + ',' + end + ',' + dist + ',' + routes.i + ',' + parseInt(routes.j + k - 1));
          dests.push(end);
        }
      }
      if (routes.dests.diff(dests).length > 0) {
        console.log("Missed routes from " + routes.src + " to : " + routes.dests.diff(dests).join(","));
      }
      console.log('Processed request #' + logCount + ': Found distances for ' + distances.length);
      logCount++;
      console.log("write start: src: " + routes.src + " dest: " + routes.dests[0]);
      fs.appendFileSync(OUT_FILE_PATH, distances.join('\n') + '\n');
      console.log("write end: src: " + routes.src + " dest: " + routes.dests[routes.dests.length - 1]);
      cb(null,distances);
    });
  };
  repeat_infinite();
}, function(err, results) {
});
