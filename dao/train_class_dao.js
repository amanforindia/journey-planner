var fs = require('fs');

var TrainClassDao = function() {};

/**
 * Data returned would look like ['SuperFast', '3A 2A SL']
 *
 * Types of train class: 'Garibrath', 'Duronto', 'JanShatabdi', 'Mail/Express', 'AC Express',
 *        'Rajdhani', 'Shatabdi', 'SuperFast', 'AC SuperFast', 'Sampark Kranti'.
 *
 */
var trainClasses = JSON.parse(fs.readFileSync('./static_data/train-classes-data.txt', 'utf8'));
/**
 *
 * @param train {object}
 * @return {object + type property + classes property}
 */
var getTrainClassAndSubClass = function(train) {
  if (trainClasses[train.trainNo] != undefined && trainClasses[train.trainNo] != null) {
    train.type = trainClasses[train.trainNo][0];
    train.classes = trainClasses[train.trainNo][1].split(' ');
  } else {
    // TODO : HARDCODED!
    train.type = 'Mail/Express';
    train.classes = ['SL'];
  }
  return train;
};

TrainClassDao.getTrainsClassAndSubClass = function(trainNos) {
  /*listOfTrains = listOfTrains.map(function(train) {
    return {
      startTime: listOfTrains[train],
      trainCode: train
    };
  });*/
  return Object.keys(trainNos).map(function(trainNo) {return getTrainClassAndSubClass(trainNos[trainNo])});
};

module.exports = TrainClassDao;
