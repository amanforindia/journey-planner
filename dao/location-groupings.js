/**
 * Created by grijesh on 17/03/16.
 */

var async = require('async');

var db = require('../connectors/cache_db.js');
var cql_statements = require('../util/cql_statements.js');

// Private shared variable
var db_connection = db.getInstance();
var cql_queries = cql_statements.IN_STATION_GROUPING;

var StationLocationGrouping = function() {};

/**
 * @param {Array} params
 * @param {function} callback
 */
StationLocationGrouping.getLocationOfStation = function(params, callback) {
    async.series([function(callback) {
        var search_query = cql_queries.SEL['getLocation'];
        db_connection.execute(search_query, params, { prepare: true }, function(err, result) {
            var return_obj;
            if (!err) {
                return_obj = {
                    'results': result.rows
                };
            }
            callback(err, return_obj);
        });
    }], function(err, results) {
        db_connection.shutdown(callback(results));
    });
};

/**
 * @param {Array} params
 * @param {function} callback
 */
StationLocationGrouping.getStationCodesUsingLocation = function(params, callback) {
    async.series([function(callback) {
        var search_query = cql_queries.SEL['getListOfStationCodes'];
        db_connection.execute(search_query, params, { prepare: true }, function(err, result) {
            var return_obj;
            if (!err) {
                return_obj = {
                    'results': result.rows
                };
            }
            callback(err, return_obj);
        });
    }], function(err, results) {
        callback(results);
    });
};

module.exports = StationLocationGrouping;
