var fs = require('fs');

var TrainFairDao = function() {};

/**
 * Classes of Train: Classes of Birth
 * ACServices: '2APeak', '1APeak', 'CC', '1ALean', '2ALean', '3A'
 * YuvaCC: 'Other', '18-35'
 * Shatabdi: 'CC', 'EC'
 * Rajdhani: '1A', '3A', '2A'
 * Garibrath: 'CC', '3A'
 * Suburban: 'GN', 'FC'
 * Mail/Express: 'FC', 'SL', '2S;
 * Ordinary: 'FC', 'SL', '2S'
 * JanShatabdi: 'CC', '2S'
 *
 * 1,Feb - 31,March and 1,Aug - 31,Aug ---> Lean
 * Rest of the year ---> Peak
*/
var trainFares = JSON.parse(fs.readFileSync('./static_data/train-fair-data.txt', 'utf8'));
  /**
   *
   * @param listOfTrips {trainClass, birthClass, dist}
   */
var getTrainFairForClass = function(train, birthClass) {
  var span;
  if (train.distance <= 300) {
    span = 5;
  } else if (train.distance <= 1000) {
    span = 10
  } else if (train.distance <= 2500) {
    span = 25
  } else {
    span = 50
  }
  var type = train.type;
  // TODO: lean/peak handle correctly
  var classMap = {
    '2A': '2APeak',
    '1A': '1APeak',
    '3A': '3A',
    'CC': 'CC',
    'SL': 'SL',
    'FC': 'FC',
    '2S': '2S',
    'GN': 'GN',
    'Ex': 'EC'
  };
  var fare;
  switch(type) {
    case 'Duronto':
      if (birthClass != 'SL') {
        fare = trainFares['Rajdhani'][birthClass][Math.floor(train.distance/span) * span];
      } else {
        fare = trainFares['Mail/Express'][classMap[birthClass]][Math.floor(train.distance/span) * span];
      }
      break;
    case 'Sampark Kranti':
    case 'SuperFast':
    case 'AC Express':
    case 'Mail/Express':
    case 'AC SuperFast':
      if (Object.keys(trainFares['Mail/Express']).indexOf(classMap[birthClass]) > -1) {
        fare = trainFares['Mail/Express'][classMap[birthClass]][Math.floor(train.distance/span) * span];
      } else {
        fare = trainFares['ACServices'][classMap[birthClass]][Math.floor(train.distance/span) * span];
      }
      break;
    case 'Rajdhani':
      fare = trainFares['Rajdhani'][birthClass][Math.floor(train.distance/span) * span];
      break;
    default:
      fare = trainFares
        [train.type]
        [classMap[birthClass]]
        [Math.floor(train.distance/span) * span];
  }
  return fare;
};

TrainFairDao.getTrainFare = function(train) {
  var classes = train.classes;
  var prices = classes.map(function(birthClass) {
    return getTrainFairForClass(train, birthClass);
  });
  delete train.classes;
  train.fares = {};
  for(var i=0;i<classes.length;i++) {
    train.fares[classes[i]] = prices[i];
  }
  return train;
};

module.exports = TrainFairDao;
