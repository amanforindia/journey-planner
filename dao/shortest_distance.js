var async = require('async');
var trim = require('trim');

var db = require('../connectors/cache_db.js');
var cql_statements = require('../util/cql_statements.js');
var logger = require("../util/logger.js").getInstance();

// Private shared variable
var db_connection = db.getInstance();
var cql_queries = cql_statements.SHORTEST_DISTANCE;

var ShortestDistance = function() {};

/**
 * Get shortest distance between two stations
 * @param {string} origin_station
 * @param {string} destination_station
 * @param {function} callback
 */
ShortestDistance.getShortestDistance = function(origin_station, destination_station, callback) {
    async.series([function (callback) {
        var search_query = cql_queries.SELECT;
        var param_array = [trim(origin_station.toUpperCase()), trim(destination_station.toUpperCase())].sort();
        db_connection.execute(search_query, param_array, {prepare: true}, function (err, result) {
            var min_distance;
            if (!err) {
                if (result.rows[0]) {
                    min_distance = result.rows[0].min_distance
                } else {
                    min_distance = undefined;
                }
            } else {
                logger.error('Failed to retrieve shortest distance data: ' + err);
            }
            callback(err, min_distance);
        });
    }], function (err, results) {
      if (!err) {
        callback(results[0]);
      }
    });
};

module.exports = ShortestDistance;
