var async = require('async');

var db = require('../connectors/cache_db.js');
var cql_statements = require('../util/cql_statements.js');
var Util = require('../util/utility_functions.js');

// Private shared variable
var db_connection = db.getInstance();
var cql_queries = cql_statements.IN_TRAIN_AVAIBILITY_MAP;

var TrainAvailability = function() {};

/**
 * Get cached availability status for a train
 * @param {function} callback
 * @param {number} start_date
 * @param {string=} opt_origin_station_code
 * @param {string=} opt_destination_station_code
 * @param {string=} opt_travel_class
 * @param {string=} opt_train_code
 */
TrainAvailability.getAvailability = function(callback, start_date, opt_origin_station_code,
                                             opt_destination_station_code, opt_travel_class, opt_train_code) {
    var params = [start_date];
    Util.appendIterativelyToArrayIfNotNull(params, [opt_origin_station_code, opt_destination_station_code,
      opt_travel_class, opt_train_code]);
    async.series([function (callback) {
        var search_query = cql_queries.SELECT[params.length.toString()];
        db_connection.execute(search_query, params, {prepare: true}, function (err, result) {
            var return_obj;
            if (!err) {
                return_obj = {
                    'source': 'CACHE',
                    'timestamp': JSON.stringify(Date.now()),
                    'number_of_results': result.rows.length,
                    'results': result.rows
                };
            }
            callback(err, return_obj);
        });
    }], function (err, results) {
        callback(results);
    });
};

/**
 * Update the availability data
 * @param {number} start_date
 * @param {string} origin_station_code
 * @param {string} destination_station_code
 * @param {string} travel_class
 * @param {string} train_code
 * @param {function} opt_callback
 */
TrainAvailability.updateAvailabilityData = function(start_date, origin_station_code, destination_station_code,
                                                    travel_class, train_code, opt_callback) {
    var params = [start_date, origin_station_code, destination_station_code, travel_class, train_code];
    async.series([function(callback) {
        var insert_query = cql_queries.INSERT;
        db_connection.execute(insert_query, params, { prepare: true }, function(err) {
            if (err) {
                console.log(err);
            }
            callback(err);
        });
    }], function(err) {
      if (opt_callback) {
        opt_callback(err);
      }
    })
};

module.exports = TrainAvailability;
