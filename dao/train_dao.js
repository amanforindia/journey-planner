var db = require('../connectors/cache_db.js').getInstance();
var cql_statements = require('../util/cql_statements.js');
var cql_queries = cql_statements.TRAIN_DETAIL;
var logger = require('../util/logger.js').getInstance();

var TrainDao = {};

TrainDao.getDetails = function(trainCodes, cb) {
  var search_query = cql_queries.SELECT;
  db.execute(search_query, trainCodes, {prepare: true}, function(err, res) {
    if(err) {
      logger.error(JSON.stringify(err));
      cb(null);
    } else {
      cb(res);
    }
  });
};
