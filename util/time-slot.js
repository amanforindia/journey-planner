var assert = require('assert');

// Reference date is 1st September 1991 which lies on Sunday (0th Day)
var _DEFAULT_YEAR = 1991;
var _DEFAULT_MONTH = 8;
var _DEFAULT_DAY = 1;

/**
 * @param {number} hours
 * @param {number} minutes
 * @param {number} seconds
 * @param {number=} opt_dayofweek ranges from 0 to 6 0 being sunday
 * @constructor
 */
var TimeSlot = function (hours, minutes, seconds, opt_dayofweek) {
  this._date_object = new Date(_DEFAULT_YEAR, _DEFAULT_MONTH, opt_dayofweek ? opt_dayofweek + 1 : _DEFAULT_DAY, hours, minutes, seconds);
};

TimeSlot.prototype.getTimeSlot = function () {
  return TimeSlot.getSlot(this._date_object.getHours(), this._date_object.getMinutes(), this._date_object.getSeconds());
};

/**
 * Return a json object for the time slot
 * @returns {{hours: number, minutes: number, seconds: number}}
 */
TimeSlot.prototype.getActualTime = function () {
  return {
    hours: this._date_object.getHours(),
    minutes: this._date_object.getMinutes(),
    seconds: this._date_object.getSeconds()
  };
};

/**
 * @returns {number}
 */
TimeSlot.prototype.getDayOfWeek = function () {
  return this._date_object.getDay();
};

/**
 * The slot interval is 2 hours. If this is changed the logic will get complicated.
 * @param hours
 * @param minutes
 * @param seconds
 * @returns {{from, to}}
 */
TimeSlot.getSlot = function (hours, minutes, seconds) {
  var start_hour, end_hour;
  if (hours % 2 === 0) {
    start_hour = hours;
    end_hour = hours + 2;
    end_hour = (end_hour === 24 ? 0 : end_hour) - 1;
    end_hour = end_hour >= 0 ? end_hour : 23;
  } else {
    start_hour = hours - 1;
    end_hour = hours;
  }
  return {
    from: TimeSlot.format(start_hour, 0, 0),
    to: TimeSlot.format(end_hour, 59, 59)
  }
};

/**
 * Format from date object to get the time as number that is accepted by backend
 * @param {Date} date_obj
 * @returns {number}
 */
TimeSlot.formatFromDateObject = function (date_obj) {
  return TimeSlot.format(date_obj.getHours(), date_obj.getMinutes(), date_obj.getSeconds());
};

/**
 * Formats a given string to a number which can be used in queries in graph db
 * @param {string} time_str
 * @returns {number}
 */
TimeSlot.formatFromString = function (time_str) {
  var time_arr = time_str.split(':');
  var hour_time = parseInt(time_arr[0], 10);
  var minute_time = parseInt(time_arr[1], 10);
  var second_time = parseInt(time_arr[2], 10);

  // Hour time should not be undefined
  assert(!isNaN(hour_time));

  // If minute or second is NaN then default them to 0
  minute_time = isNaN(minute_time) ? 0 : minute_time;
  second_time = isNaN(second_time) ? 0 : second_time;

  return TimeSlot.format(hour_time, minute_time, second_time);
};

/**
 * Formats a given hours minutes and seconds to a number which can be used in queries in graph db
 * @param {number} hours
 * @param {number} minutes
 * @param {number} seconds
 * @returns {number}
 */
TimeSlot.format = function (hours, minutes, seconds) {
  assert(hours < 24 && hours >= 0 && minutes < 60 && minutes >= 0 && seconds < 60 && seconds >= 0);
  return hours * 3600 + minutes * 60 + seconds;
};

/**
 * Parses a string in the form (hh:mm:ss) representation of time into TimeSlot object
 * @param {string} time_as_str
 * @returns {TimeSlot}
 */
TimeSlot.parse = function (time_as_str) {
  var time_arr = time_as_str.split(':');
  var hour_time = parseInt(time_arr[0], 10);
  var minute_time = parseInt(time_arr[1], 10);
  var second_time = parseInt(time_arr[2], 10);

  // Hour time should not be undefined
  assert(!isNaN(hour_time));

  // If minute or second is NaN then default them to 0
  minute_time = isNaN(minute_time) ? 0 : minute_time;
  second_time = isNaN(second_time) ? 0 : second_time;

  return new TimeSlot(hour_time, minute_time, second_time);
};

/**
 * Returns a new time slot from a date obj
 * @param {Date} date_obj
 * @returns {TimeSlot}
 */
TimeSlot.parseFromDateObj = function (date_obj) {
  return new TimeSlot(date_obj.getHours(), date_obj.getMinutes(), date_obj.getSeconds(), date_obj.getDay());
};

module.exports = TimeSlot;
