var CypherStatements = function() {};

/****** All cypher statements for neo4j db are here *******/

CypherStatements.NEO4J = {
    CREATE_STATION: "MERGE (stn:Station {code: {station}.code, name: {station}.name}) RETURN stn",
    CREATE_CONSTRAINT_ON_STATION: "CREATE CONSTRAINT ON (stn: Station) ASSERT stn.code IS UNIQUE",
    CREATE_STATION_DATETIME_RELATION: "MATCH (start:Station {code: {start_station}.code}) " +
        "MATCH (end:Station {code: {end_station}.code}) " +
        "MERGE (dayofweek: DayOfWeek {station: {day_data}.station, day: {day_data}.day}) " +
        "CREATE UNIQUE (start)-[:RUNS_ON]->(dayofweek) " +
        "MERGE (time: TimeSlot {from: {slotted_time}.from, to: {slotted_time}.to, train_code: {slotted_time}.train_code, station: {slotted_time}.station}) " +
        "CREATE UNIQUE (dayofweek)-[:RUNS_BETWEEN]->(time) " +
        "CREATE UNIQUE (time)-[:CONNECTED_TO {train}]->(end)"
};

module.exports = CypherStatements;