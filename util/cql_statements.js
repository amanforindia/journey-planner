var CQLStatements = function() {};

/****** All cql statements for cache_strategy.in_train_availability_map *******/

CQLStatements.IN_TRAIN_AVAIBILITY_MAP = {};

/**
 * @type {{number: string}} number represents the number of parameters for select(key) value represents the prepared statement
 */
CQLStatements.IN_TRAIN_AVAIBILITY_MAP.SELECT = {
    '5': "SELECT * from in_train_availability_map where start_date=? and origin_station_code=? and " +
        "destination_station_code=? and travel_class=? and train_code=?",
    '4': "SELECT * from in_train_availability_map where start_date=? and origin_station_code=? and " +
        "destination_station_code=? and travel_class=?",
    '3': "SELECT * from in_train_availability_map where start_date=? and origin_station_code=? and " +
        "destination_station_code=?",
    '2': "SELECT * from in_train_availability_map where start_date=? and origin_station_code=?",
    '1': "SELECT * from in_train_availability_map where start_date=?"
};

CQLStatements.IN_TRAIN_AVAIBILITY_MAP.INSERT = "INSERT INTO in_train_availability_map (start_date, origin_station_code, " +
  "destination_station_code, travel_class, train_code, availability_msg, availability_status, update_time) " +
  "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

CQLStatements.IN_STATION_GROUPING = {};

CQLStatements.IN_STATION_GROUPING.SEL = {
    'getLocation': "SELECT * from in_station_grouping_codes where station_code=?",
    'getListOfStationCodes': "SELECT * from in_station_grouping_loc where location=?"
};

CQLStatements.SHORTEST_DISTANCE = {};

CQLStatements.SHORTEST_DISTANCE.SELECT = "SELECT min_distance from in_shortest_distance_stations where station_a=? and station_b=?";

CQLStatements.SHORTEST_DISTANCE.INSERT = "INSERT INTO in_shortest_distance_stations (station_a, station_b, min_distance) values (?, ?, ?)";

CQLStatements.TRAIN_DETAIL = {};

CQLStatements.TRAIN_DETAIL.SELECT = "SELECT * from train_detail where train_code=?";

module.exports = CQLStatements;
