var StringBuilder = require("stringbuilder");
var IdGenerator = require("./id_generator.js");
var Util = require("./utility_functions.js");
var DistanceTransformer = require("./distance_transformer.js");
var ShortestDistance = require("../dao/shortest_distance.js");

/**
 * Initializes the query builder object for building cypher queries for the neo4j model
 *
 * @param{CypherQueryBuilder.ORDER_STRATEGY=} opt_orderStrategy
 * @constructor
 */
var CypherQueryBuilder = function (opt_orderStrategy) {
  this._queryconstructor = [];

  this._returnVariables = [];

  this._lastCommitedType = null;

  this._min_waiting_time = null;

  this._orderStrategy = opt_orderStrategy;

  this._idGenerator = new IdGenerator();

  this._nodes = {
    train: [],
    station: [],
    station_id: [],
    dayOfWeek: [],
    timeSlot: []
  };
};

/**
 * Number of seconds in a day
 * @type {number}
 * @private
 */
CypherQueryBuilder._SECONDS_IN_DAY = 86400;

CypherQueryBuilder._TYPE = {
  START: "start",
  RUNS_ON: "runs_on",
  RUNS_BETWEEN: "runs_between",
  CONNECTED_TO: "connected_to"
};

/**
 * Specify what order the result should be in
 * @enum {number}
 */
CypherQueryBuilder.ORDER_STRATEGY = {
  NONE: 0,
  DISTANCE: 1,
  TRAVEL_TIME: 2
};

/**
 * Enum representing days of week
 * @enum {number}
 */
CypherQueryBuilder.DAY_OF_WEEK = {
  SUNDAY: 0,
  MONDAY: 1,
  TUESDAY: 2,
  WEDNESDAY: 3,
  THURSDAY: 4,
  FRIDAY: 5,
  SATURDAY: 6
};

/**
 * @param {string} stationCode
 * @param {boolean} is_return if true will return the start station
 * @returns {CypherQueryBuilder}
 */
CypherQueryBuilder.prototype.start = function (stationCode, is_return) {
  var payload_obj = Util.isDefAndNotNull(stationCode) ? {code: stationCode} : {};
  this._queryconstructor.push({
    type: CypherQueryBuilder._TYPE.START,
    payload: payload_obj,
    is_return: is_return
  });
  this._nodes.station.push(stationCode);
  return this;
};

/**
 * @param {number|null} day_of_week
 * @param {boolean} is_return if true will return this value
 * @returns {CypherQueryBuilder}
 */
CypherQueryBuilder.prototype.runs_on = function (day_of_week, is_return) {
  var payload_obj = Util.isDefAndNotNull(day_of_week) ? {day: day_of_week} : {};
  this._queryconstructor.push({
    type: CypherQueryBuilder._TYPE.RUNS_ON,
    payload: payload_obj,
    is_return: is_return
  });
  return this;
};

/**
 * @param {TimeSlot|null} time_slot
 * @param {boolean} is_return if true will return
 * @returns {CypherQueryBuilder}
 */
CypherQueryBuilder.prototype.runs_between = function (time_slot, is_return) {
  var payload_obj = Util.isDefAndNotNull(time_slot) ? time_slot.getTimeSlot() : {};
  this._queryconstructor.push({
    type: CypherQueryBuilder._TYPE.RUNS_BETWEEN,
    payload: payload_obj,
    is_return: is_return
  });
  return this;
};

/**
 * @param {string|null} stationCode
 * @param {boolean} is_return if true will return the start station
 * @returns {CypherQueryBuilder}
 */
CypherQueryBuilder.prototype.connected_to = function (stationCode, is_return) {
  var payload_obj = Util.isDefAndNotNull(stationCode) ? {code: stationCode} : {};
  this._queryconstructor.push({
    type: CypherQueryBuilder._TYPE.CONNECTED_TO,
    payload: payload_obj,
    is_return: is_return
  });
  this._nodes.station.push(stationCode);
  return this;
};

/**
 * @param {null|undefined|number=} page_size The number of results to load
 * @param {null|undefined|number=} offset The number of results already loaded
 * @param {null|undefined|number=} min_waiting_time In number of seconds
 * @param {function(string, string)} callback takes 2 parameters err and result
 */
CypherQueryBuilder.prototype.build = function (page_size, offset, min_waiting_time, callback) {
  var query_builder = new StringBuilder();
  this._min_waiting_time = min_waiting_time;
  // TODO: More cases should be handled here.
  for (var i = 0; i < this._queryconstructor.length; i++) {
    var ele = this._queryconstructor[i];
    this._compile_missing(ele.type, query_builder);
    if (ele.type === CypherQueryBuilder._TYPE.START) {
      this._compile_start(ele.payload, ele.is_return, query_builder);
    } else if (ele.type === CypherQueryBuilder._TYPE.RUNS_ON) {
      this._compile_runs_on(ele.payload, ele.is_return, query_builder);
    } else if (ele.type === CypherQueryBuilder._TYPE.RUNS_BETWEEN) {
      this._compile_runs_between(ele.payload, ele.is_return, query_builder);
    } else if (ele.type === CypherQueryBuilder._TYPE.CONNECTED_TO) {
      this._compile_connected_to(ele.payload, ele.is_return, query_builder);
    }
  }
  ShortestDistance.getShortestDistance(this._nodes.station[0], Util.getLastElementFromArray(this._nodes.station),
    this._pre_build.bind(this, callback, query_builder, page_size, offset));

};

CypherQueryBuilder.prototype._pre_build = function (callback, query_builder, page_size, offset, shortest_distance) {
  // TODO: Divide the queries into parallel queries by using where clause(example: same day departure or next day departure)
  this._compile_conditions(query_builder, DistanceTransformer.simpleTransform(shortest_distance));

  // Add what should be returned here
  this._compile_return(query_builder);

  // Compile order by clause
  this._compile_order_by(query_builder);

  // Compile page size and offset
  this._compile_page_size(query_builder, page_size, offset);

  query_builder.build(callback);
};

CypherQueryBuilder.prototype._compile_conditions = function (querybuilder, shortest_distance) {
  querybuilder.append(" where ");
  var timeConstraintBuilder = new StringBuilder();
  var distanceConstraintBuilder = new StringBuilder();
  var dayConstraintBuilder = new StringBuilder();
  // Compile the constraints
  this._nodes.train.forEach(function (assigned_train_id, idx, arr) {
    // Distance constraints
    var distanceAttr = assigned_train_id + ".distance";
    idx > 0 ? distanceConstraintBuilder.append(" + " + distanceAttr) : distanceConstraintBuilder.append(distanceAttr);

    // Day constraint
    if (idx > 0) {
      // Compile Day constraint only when there are more than 1 trains and start with the day for 2nd train journey
      var day_idx_to_compile = idx;
      if (CypherQueryBuilder._isStringBuilderNotEmpty(dayConstraintBuilder)) {
        dayConstraintBuilder.append(" and ");
      }
      dayConstraintBuilder.append(this._nodes.dayOfWeek[day_idx_to_compile] + ".day = ");
      // Add the expression for getting the correct day
      dayConstraintBuilder.append("(" + this._nodes.dayOfWeek[day_idx_to_compile - 1] + ".day + " +
        arr[idx - 1] + ".arrival_day - " + arr[idx -1] + ".departure_day" + ") % 7");
    }

    // Time constraints
    if (idx > 0) {
      if (CypherQueryBuilder._isStringBuilderNotEmpty(timeConstraintBuilder)) {
        timeConstraintBuilder.append(" and ");
      }
      timeConstraintBuilder.append(assigned_train_id);
      timeConstraintBuilder.append(".source_departure_time > ");
      timeConstraintBuilder.append(arr[idx - 1]);
      timeConstraintBuilder.append(".destination_arrival_time");
      if (Util.isDefAndNotNull(this._min_waiting_time)) {
        timeConstraintBuilder.append(" + " + this._min_waiting_time);
      }
    }
  }.bind(this));

  // Add the distance constraint
  distanceConstraintBuilder.append(" < " + shortest_distance);

  // Add all builders together
  querybuilder.append(distanceConstraintBuilder);
  if (CypherQueryBuilder._isStringBuilderNotEmpty(distanceConstraintBuilder) &&
    (CypherQueryBuilder._isStringBuilderNotEmpty(dayConstraintBuilder) ||
    CypherQueryBuilder._isStringBuilderNotEmpty(timeConstraintBuilder))) {
    querybuilder.append(" and ");
  }
  querybuilder.append(dayConstraintBuilder);
  if (CypherQueryBuilder._isStringBuilderNotEmpty(dayConstraintBuilder) &&
    CypherQueryBuilder._isStringBuilderNotEmpty(timeConstraintBuilder)) {
    querybuilder.append(" and ");
  }
  querybuilder.append(timeConstraintBuilder);
};

CypherQueryBuilder.prototype._compile_return = function (querybuilder) {
  if (this._returnVariables.length > 0) {
    querybuilder.append(" return");
    for (var i = 0; i < this._returnVariables.length; i++) {
      if (i > 0) {
        querybuilder.append(",");
      }
      querybuilder.append(" ");
      querybuilder.append(this._returnVariables[i]);
    }
  }
};

CypherQueryBuilder.prototype._compile_order_by = function(queryBuilder) {
  if (this._orderStrategy === CypherQueryBuilder.ORDER_STRATEGY.DISTANCE) {
    queryBuilder.append(" order by ");
    this._nodes.train.forEach(function(assigned_train_id, idx) {
      var queryStr = assigned_train_id + ".distance";
      idx > 0 ? queryBuilder.append(" + " + queryStr) : queryBuilder.append(queryStr);
    });
  }
  if (this._orderStrategy === CypherQueryBuilder.ORDER_STRATEGY.TRAVEL_TIME) {
    // In this strategy to reduce computational complexity for the query we assume a logical
    // assumption that the total travel time will never exceed a week(i.e. 7 days)
    // Formula: cx + (n.dst - 1.dst) where c is a constant(number of seconds in a day),
    // x is number of days for travel n.dst is destination arrival time for last train
    // and 1.sdt is source departure time for 1st train
    queryBuilder.append(" order by (" + CypherQueryBuilder._SECONDS_IN_DAY + " * ");
    queryBuilder.append("((" + Util.getLastElementFromArray(this._nodes.dayOfWeek) + ".day + ");
    queryBuilder.append(Util.getLastElementFromArray(this._nodes.train) + ".arrival_day - ");
    queryBuilder.append(Util.getLastElementFromArray(this._nodes.train) + ".departure_day - ");
    queryBuilder.append(this._nodes.dayOfWeek[0] + ".day + 7 ) % 7 ) + ");
    queryBuilder.append(Util.getLastElementFromArray(this._nodes.train) + ".destination_arrival_time - ");
    queryBuilder.append(this._nodes.train[0] + ".source_departure_time)");
  }
};

CypherQueryBuilder.prototype._compile_page_size = function(queryBuilder, page_size, offset) {
  if (Util.isDefAndNotNull(offset) && offset >= 0) {
    queryBuilder.append(" skip " + offset);
  }
  if (Util.isDefAndNotNull(page_size) && page_size >= 0) {
    queryBuilder.append(" limit " + page_size);
  }
};

CypherQueryBuilder.prototype._compile_missing = function (type, querybuilder) {
  var cypherType = CypherQueryBuilder._TYPE;
  var prevType = this._lastCommitedType;
  if (type === cypherType.RUNS_ON && prevType === cypherType.RUNS_BETWEEN) {
    this._compile_connected_to({}, true, querybuilder);
  } else if (type === cypherType.RUNS_BETWEEN && prevType === cypherType.CONNECTED_TO) {
    this._compile_runs_on({}, true, querybuilder);
  } else if (type === cypherType.CONNECTED_TO && prevType === cypherType.RUNS_ON) {
    this._compile_runs_between({}, false, querybuilder);
  } else if (type === cypherType.CONNECTED_TO && (prevType === cypherType.START ||
    prevType === cypherType.CONNECTED_TO)) {
    this._compile_runs_on({}, true, querybuilder);
    this._compile_runs_between({}, false, querybuilder);
  }
};

CypherQueryBuilder.prototype._compile_start = function (payload, is_return, querybuilder) {
  this._lastCommitedType = CypherQueryBuilder._TYPE.START;
  var identifier = this._idGenerator.getNextUniqueId();
  this._nodes.station_id.push(identifier);
  if (is_return) {
    this._returnVariables.push(identifier);
  }

  querybuilder.append("MATCH (");
  querybuilder.append(identifier);
  querybuilder.append(":Station ");
  querybuilder.append(CypherQueryBuilder._stringify(payload));
  querybuilder.append(")");
};

CypherQueryBuilder.prototype._compile_runs_on = function (payload, is_return, querybuilder) {
  this._lastCommitedType = CypherQueryBuilder._TYPE.RUNS_ON;
  var identifier = this._idGenerator.getNextUniqueId();
  this._nodes.dayOfWeek.push(identifier);
  if (is_return) {
    this._returnVariables.push(identifier);
  }

  querybuilder.append("-[:RUNS_ON]->");
  querybuilder.append("(");
  querybuilder.append(identifier);
  querybuilder.append(":DayOfWeek ");
  querybuilder.append(CypherQueryBuilder._stringify(payload));
  querybuilder.append(")");
};

CypherQueryBuilder.prototype._compile_runs_between = function (payload, is_return, querybuilder) {
  this._lastCommitedType = CypherQueryBuilder._TYPE.RUNS_BETWEEN;
  var identifier = this._idGenerator.getNextUniqueId();
  this._nodes.timeSlot.push(identifier);
  if (is_return) {
    this._returnVariables.push(identifier);
  }

  querybuilder.append("-[:RUNS_BETWEEN]->");
  querybuilder.append("(");
  querybuilder.append(identifier);
  querybuilder.append(":TimeSlot ");
  querybuilder.append(CypherQueryBuilder._stringify(payload));
  querybuilder.append(")");
};

CypherQueryBuilder.prototype._compile_connected_to = function (payload, is_return, querybuilder) {
  this._lastCommitedType = CypherQueryBuilder._TYPE.CONNECTED_TO;
  var trainIdentifier = this._idGenerator.getNextUniqueId();
  var stnIdentifier = this._idGenerator.getNextUniqueId();
  this._nodes.train.push(trainIdentifier);
  this._nodes.station_id.push(stnIdentifier);
  if (is_return) {
    this._returnVariables.push(trainIdentifier);
    this._returnVariables.push(stnIdentifier);
  }

  querybuilder.append("-[");
  querybuilder.append(trainIdentifier);
  querybuilder.append(":CONNECTED_TO]->");
  querybuilder.append("(");
  querybuilder.append(stnIdentifier);
  querybuilder.append(":Station ");
  querybuilder.append(CypherQueryBuilder._stringify(payload));
  querybuilder.append(")");
};

CypherQueryBuilder._stringify = function (obj) {
  if (!Util.isObjectEmpty(obj)) {
    var objBuilder = new StringBuilder();
    objBuilder.append("{");
    for (var key in obj) {
      var isNumberType = Util.isNumber(obj[key]);
      objBuilder.append(key);
      objBuilder.append(": ");
      if (!isNumberType) {
        objBuilder.append("'");
      }
      objBuilder.append(obj[key]);
      if (!isNumberType) {
        objBuilder.append("'");
      }
    }
    objBuilder.append("}");
    return objBuilder;
  }
  return "";
};

CypherQueryBuilder._isStringBuilderNotEmpty = function (strBuilder) {
  return strBuilder.instructions && strBuilder.instructions.length > 0
};

module.exports = CypherQueryBuilder;
