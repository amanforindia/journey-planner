var _generator = null;

var IdGenerator = function() {};

IdGenerator.getInstance = function() {
    if (_generator !== null) {
        return _generator;
    }
    _generator = new IdGenerator();
    return _generator;
};

IdGenerator.prototype._next_id = 0;

IdGenerator.prototype.getNextUniqueId = function() {
    return "id" + (this._next_id++).toString(36);
};

module.exports = IdGenerator;
