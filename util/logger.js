var moment = require("moment");
var logger = require("winston");

var Logger = {};

logger.add(logger.transports.File, { filename: "./log/" + moment().format("YYYY_MM_DD") + ".log" });
logger.remove(logger.transports.Console);

Logger.getInstance = function() {
  return logger;
};

module.exports = Logger;
