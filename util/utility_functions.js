var UtilityFunctions = function() {};

/**
 * Returns true if the value is an number
 * @param {*} value
 * @returns {boolean}
 */
UtilityFunctions.isNumber = function(value) {
    return typeof(value) == "number"
};

/**
 * Return true if an object is empty
 * @param obj
 * @returns {boolean}
 */
UtilityFunctions.isObjectEmpty = function(obj) {
    for (var key in obj) {
        return false;
    }
    return true;
};

/**
 * Return true if val id defined an not null
 * @param val
 * @returns {boolean}
 */
UtilityFunctions.isDefAndNotNull = function(val) {
    return val != null;
};

/**
 * Return true if string is empty or contains only whitespaces
 * @param str
 * @returns {boolean}
 */
UtilityFunctions.isStringEmpty = function(str) {
    return /^[\s\xa0]*$/.test(str);
};

/**
 * @type {Array} arr
 */
UtilityFunctions.getLastElementFromArray = function(arr) {
    return arr[arr.length - 1];
};

/**
 * Append an obj to arr if obj is defined and not null;
 * @param {Array} arr
 * @param {Object} obj
 */
UtilityFunctions.appendToArrayIfNotNull = function(arr, obj) {
  if (UtilityFunctions.isDefAndNotNull(obj)) {
    arr.push(obj);
  }
};

/**
 * Append items from a list to an arr if item is defined and not null;
 * @param {Array} arr
 * @param {Array} obj
 */
UtilityFunctions.appendIterativelyToArrayIfNotNull = function(arr, items) {
  for (var item in items) {
    UtilityFunctions.appendToArrayIfNotNull(arr, items[item]);
  }
};

module.exports = UtilityFunctions;
