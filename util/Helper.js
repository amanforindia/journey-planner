var moment = require('moment');
Helper = {};

/**
 *
 * @param dateTimestamp timestamp in long (millis) from which to get start of day.
 * @param additionalSeconds Seconds to be added (current format is number of seconds from midnight)
 */
Helper.getTimeFromDateAndHours = function(dateTimestamp, additionalSeconds, departure_day, train_departure_day, train_arrival_day) {
  var secs = additionalSeconds;
  var ret = moment(dateTimestamp).startOf('day').add(departure_day - moment(dateTimestamp).day() >= 0 ?
    departure_day - moment(dateTimestamp).day() : (departure_day - moment(dateTimestamp).day() + 7), 'days')
    .add(secs, 'seconds');
  if (train_departure_day != null && train_arrival_day) {
    return ret.add(train_arrival_day - train_departure_day, 'days').valueOf();
  } else {
    return ret.valueOf();
  }
};

module.exports = Helper;
