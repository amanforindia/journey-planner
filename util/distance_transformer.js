var DistanceTransformer = function() {};

/**
 * Transforms the shortest distance so that the model can have an upper bound to the result
 * @param {number} min_distance
 * @returns {number|null}
 */
DistanceTransformer.simpleTransform = function(min_distance) {
    if (min_distance) {
        return 1.75 * min_distance;
    } else {
        return null;
    }
};

module.exports = DistanceTransformer;
