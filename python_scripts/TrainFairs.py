import csv
import json

trainFairs = {'Suburban' : {'GN': {}, 'FC': {}},
              'Ordinary' : {'2S': {}, 'SL': {}, 'FC': {}},
              'Mail/Express' : {'2S': {}, 'SL': {}, 'FC': {}},
              'ACServices' : {'CC': {}, '3A': {}, '2ALean': {}, '2APeak': {}, '1ALean': {}, '1APeak': {}},
              'Garibrath' : {'CC': {}, '3A': {}},
              'Rajdhani' : {'3A': {}, '2A': {}, '1A': {}},
              'Shatabdi' : {'CC': {}, 'EC': {}},
              'JanShatabdi' : {'2S': {}, 'CC': {}},
              'YuvaCC' : {'Other': {}, '18-35': {}}
              }

def addEntryToFairs(L):
    key = L[0].split('-')[1]
    trainFairs['Suburban']['GN'][key] = L[1]
    trainFairs['Suburban']['FC'][key] = L[2]
    trainFairs['Ordinary']['2S'][key] = L[4]
    trainFairs['Ordinary']['SL'][key] = L[6]
    trainFairs['Ordinary']['FC'][key] = L[8]
    trainFairs['Mail/Express']['2S'][key] = L[10]
    trainFairs['Mail/Express']['SL'][key] = L[12]
    trainFairs['Mail/Express']['FC'][key] = L[14]
    trainFairs['ACServices']['CC'][key] = L[16]
    trainFairs['ACServices']['3A'][key] = L[18]
    trainFairs['ACServices']['2ALean'][key] = L[20]
    trainFairs['ACServices']['2APeak'][key] = L[22]
    trainFairs['ACServices']['1ALean'][key] = L[24]
    trainFairs['ACServices']['1APeak'][key] = L[26]
    trainFairs['Garibrath']['CC'][key] = L[28]
    trainFairs['Garibrath']['3A'][key] = L[30]
    trainFairs['Rajdhani']['3A'][key] = L[32]
    trainFairs['Rajdhani']['2A'][key] = L[34]
    trainFairs['Rajdhani']['1A'][key] = L[36]
    trainFairs['Shatabdi']['CC'][key] = L[38]
    trainFairs['Shatabdi']['EC'][key] = L[40]
    trainFairs['JanShatabdi']['2S'][key] = L[42]
    trainFairs['JanShatabdi']['CC'][key] = L[44]
    trainFairs['YuvaCC']['Other'][key] = L[46]
    trainFairs['YuvaCC']['18-35'][key] = L[48]

with open('train_fair_data.csv', 'rb') as csvfile:
    data = csv.reader(csvfile, delimiter=',')
    for row in data:
        addEntryToFairs(row[0].split(','))

with open('Train-Fair-JSON.txt', 'w') as outfile:
    json.dump(trainFairs, outfile)
