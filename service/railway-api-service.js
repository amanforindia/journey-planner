/**
 * Created by ankitbhansali on 28/2/16.
 */

var config =  require('../config/config.js');
var consts =  require('../config/rail-consts.js');
var baseService = require('./base-service.js');
var HOST = config.RAILWAY_HOST;
var API_KEY = consts.properties.API_KEY;
var railwayService = {};
var SLASH = '/';


// Function to get Seat Availability
railwayService.getAvailability = function(trainNumber , fromStationCode , toStationCode ,date, classCode , quotaCode,cb){

    var pathname = railwayService.pathToCheckSeat(trainNumber , fromStationCode , toStationCode ,date, classCode , quotaCode);

    var urlObj = {host : HOST,pathname : pathname,protocol:'http'};

    baseService.get(urlObj , cb);
};

//Get Train Code from Train Name
railwayService.getStationCode = function(stationName,cb){
    var array = [consts.apitype.NAME_TO_CODE,consts.properties.STATION_NAME,stationName,API_KEY,consts.properties.API_KEY_VALUE]
    var pathname = SLASH + array.join(SLASH);

    var urlObj = {host : HOST,pathname : pathname,protocol:'http'};
    baseService.get(urlObj, cb);
};

//Path Builder
railwayService.pathToCheckSeat = function(trainNumber , fromStationCode , toStationCode ,date, classCode , quotaCode){

    var array = [consts.apitype.CHECK_SEAT,consts.properties.TRAIN_NUMBER,trainNumber,consts.properties.SOURCE,fromStationCode,consts.properties.DESTINATION
        ,toStationCode,consts.properties.DATE,date,consts.properties.CLASS_CODE,classCode,consts.properties.QUOTA,quotaCode,API_KEY
        ,consts.properties.API_KEY_VALUE];
    return SLASH + array.join(SLASH);
};

module.exports = railwayService;
