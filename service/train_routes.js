var async = require("async");
var ArrayList = require("arraylist");
var QueryBuilder = require("../util/cypher_querybuilder.js");
var GraphDb = require("../connectors/graph_db.js");
var TimeSlot = require("../util/time-slot.js");
var logger = require("../util/logger.js").getInstance();

var TrainRoutes = {};

/**
 * Minimum waiting time between trains.
 * @type {number}
 * @private
 */
TrainRoutes._DEFAULT_MIN_WAITING_TIME = 18000;

/**
 * Maximum size of results in the list. Other results will be removed.
 * @type {number}
 * @private
 */
TrainRoutes._MAXIMUM_RESULT_COUNT = 50;

/**
 * Get the routes from origin to destination with the specified number of hops.
 *
 * @param {string} origin_station_code
 * @param {string} destination_station_code
 * @param {number} timestamp
 * @param {Array.<number>} hops The results are build in parallel for all hops passed as array and returned (here no train change means 1 hop)
 * @param {function} callback
 */
TrainRoutes.getRoutes = function (origin_station_code, destination_station_code, timestamp, hops, callback) {
  var travel_date = new Date(timestamp);
  var travel_slot = TimeSlot.parseFromDateObj(travel_date);
  var getRoutesParallelExecutor = function (hop, parallel_callback) {
    var query_builder = new QueryBuilder(QueryBuilder.ORDER_STRATEGY.TRAVEL_TIME).start(origin_station_code, true)
      .runs_on(travel_slot.getDayOfWeek(), true);
    for (var hop_counter = 2; hop_counter <= hop; hop_counter++) {
      query_builder.connected_to(null, true);
    }
    query_builder.connected_to(destination_station_code, true).build(TrainRoutes._MAXIMUM_RESULT_COUNT, null,
      TrainRoutes._DEFAULT_MIN_WAITING_TIME, function (err, result) {
        if (!err) {
          logger.info("QUERY - " + result);
          GraphDb.getInstance().cypher({
            query: result,
            lean: true
          }, function (err, results) {
            if (!results) {
              results = [];
            }
            parallel_callback(err, results);
          });
        }
      });
  };
  var parallelExecutorFunctionArray = [];
  hops.forEach(function (hop) {
    parallelExecutorFunctionArray.push(getRoutesParallelExecutor.bind(this, hop));
  });
  async.parallel(parallelExecutorFunctionArray, function (err, results) {
    var allResults = new ArrayList;
    results.forEach(function (result) {
      allResults.add(result);
    });
    callback(err, allResults.toArray());
  });
};

/**
 * Get the routes from origin to destination with the specified number of hops.
 *
 * @param {string} origin_station_code
 * @param {string} destination_station_code
 * @param {number} timestamp
 * @param {number} page_size How many results to load. If undefined or -ve then load all
 * @param {number} offset How many results are already loaded
 * @param {number} hop How many hops should the result have
 * @param {function} callback
 */
TrainRoutes.getRoutesWithLazyLoad = function (origin_station_code, destination_station_code,
                                              timestamp, page_size, offset, hop, callback) {
  var travel_date = new Date(timestamp);
  var travel_slot = TimeSlot.parseFromDateObj(travel_date);
  var query_builder = new QueryBuilder(QueryBuilder.ORDER_STRATEGY.TRAVEL_TIME).start(origin_station_code, true)
    .runs_on(travel_slot.getDayOfWeek(), true);
  for (var hop_counter = 2; hop_counter <= hop; hop_counter++) {
    query_builder.connected_to(null, true);
  }
  query_builder.connected_to(destination_station_code, true).build(page_size, offset, function (err, result) {
    if (!err) {
      logger.info("QUERY - " + result);
      GraphDb.getInstance().cypher({
        query: result,
        lean: true
      }, function (err, result) {
        if (!result) {
          result = [];
        }
        callback(err, result);
      });
    }
  });
};

module.exports = TrainRoutes;
