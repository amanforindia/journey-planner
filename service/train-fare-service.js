/**
 * Created by aman on 30/4/16.
 */
var trainFareService = {};
var trainClassDao = require('../dao/train_class_dao');
var trainFareDao = require('../dao/train_fair_dao');

/**
 *
 * @param trains {key: trainCode + srcCode + ' ' + destCode, properties: startTime, distance, startEnd, trainNo}
 */
trainFareService.getFare = function(trains) {
  var trainsAndClasses = trainClassDao.getTrainsClassAndSubClass(trains);
  // for each object
  // {train no-start-end: { {'sl': 123, 'ac': 23, '3A': 567}}}
  return trainsAndClasses.map(function(trainAndClasses) {
    return trainFareDao.getTrainFare(trainAndClasses);
  });
};

module.exports = trainFareService;
