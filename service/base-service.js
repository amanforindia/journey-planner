var request = require('request');
var url = require('url');
var transform = require('jsonpath-object-transform');
var baseService = {};

baseService.getTransformed = function(urlObj, responseFormat, callback) {
    baseService.get(urlObj, function(err, body) {
      if (err) {
        callback(err);
      } else {
        callback(null, transform(body, responseFormat));
      }
    });
};

baseService.get = function(urlObj, callback) {
    console.log(url.format(urlObj));
    request(url.format(urlObj), {}, function(err,response,body){
        callback(err, JSON.parse(body));
    });
};

module.exports = baseService;
