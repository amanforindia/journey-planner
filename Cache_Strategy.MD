## Which database to use for caching? ##
  - Cassandra for caching live data.
  - NEO4J (Graph Database) for caching static data.

## What to expect from caching system? ##
  - Fast retrival for cached live status (currently only availability)
  - Fast update of the cache

## Purpose 1: ##
  - Cache avaibility status:
    1. What trains are available from station A to station B on Date D1 in Class C1.
    2. Is the train T1 that goes from station A to Station B available on Date D1 in Class C1.

## Data Model (Solving Purpose 1): ##
    1. Primary Key: start_date, origin_code, dest_code, travel_class, train_code, avail_status
    2. Partition Key: start_date (distributing data)
    3. Clustering Key: origin_code, dest_code, travel_class, train_code, avail_status (Sorting within partition)

    NOTE: This data model will be useful only when we already know the broken legs of a journey. The purpose is just to provide caching for avaibility status.

### Cache Update Strategy for above Data Model: ###
    1. Update cache when user requests the data.
    2. If the data is very old when user requests then don't show the old data and fetch new data and show that.
    3. If the data is quite recent (maybe 3-4 hours old) and acceptable show the user the data and at the same time fetch new data to update it.
  _Biggest Drawback: Initially when the number of users are very less then the expected cache hits will be very few._