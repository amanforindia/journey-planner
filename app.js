#!/usr/bin/env node

var express = require('express');
var logger = require('./util/logger');
var bodyParser = require('body-parser');
var expressWinston = require('express-winston');
var app = express();

app.use(bodyParser.json());
app.use('/', require('./routes/index'));
app.use('/api', require('./routes/api'));
//app.use(logger.getInstance());
app.use(expressWinston.logger({
  winstonInstance: logger.getInstance()
}));
app.use(express.static('app'));

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});
