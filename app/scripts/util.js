/**
 * Created by shoaib on 26/03/16.
 */
Util=function(){

};
Util.isEmptyString=function(obj){

  if(Util.isNullOrUndefined(obj)){
    return true;
  }
  if(typeof(obj)=="string" && ( obj===null || obj===undefined || obj==="")){
    return true;
  }
  return false;

};
Util.isNullOrUndefined=function(obj){

  if(obj===null || obj===undefined){
    return true;
  }
  return false;

};
Util.getHourDiff=function(startTimeStamp,endTimeStamp){
  var ms =  moment(endTimeStamp).diff(moment(startTimeStamp));
  var d = moment.duration(ms);
  //var s = Math.floor(d.asHours()) + moment.utc(ms).format("HH:mm");
  return d.asHours();
};
Util.getFormattedTime=function(timeStamp){
  var ms = moment(timeStamp);
  return ms.format("HH:mm");
};
Util.formatHour=function(hour,withMinute){
  var momentDuration=moment.duration(hour,'hours');
  if(!withMinute){
    var hours=moment.utc(momentDuration.asMilliseconds()).format("H");
    if(hour==="0"){
      return "";
    }
    return hours+" h";
  }
  if(hour <1 ){
    return moment.utc(momentDuration.asMilliseconds()).format("mm") +" m";
  }
  return   moment.utc(momentDuration.asMilliseconds()).format("H:mm ") +" h";
};
Util.formatWaitingTime=function(startTimeStamp,endTimeStamp){
  var ms =  moment(endTimeStamp).diff(moment(startTimeStamp));
  var d = moment.duration(ms);
  return d.hours() + " hr , " + d.minutes() + " mins ";

};
Util.getFormattedJourneyTime=function(journeyTime){
  var days=parseInt(journeyTime/24);
  if(days===0){
    return Util.formatHour(journeyTime,true);
  }
  var hour=journeyTime-(24*days);
  return days+"d "+Util.formatHour(hour,false);
};
Util.getDateFromTime=function(timeInMillis,format){
  var m=moment(timeInMillis);
  if(Util.isNullOrUndefined(format)){
    return m.format('DD MMM YYYY')
  }
  return m.format(format);
};


