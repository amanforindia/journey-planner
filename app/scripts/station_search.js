/**
 * Created by shoaib on 26/03/16.
 */
var StationSearch=function(data,limit) {
  this.data=data;
  this.limit=limit;
};
StationSearch.prototype.setData = function(data) {
  this.data=data;
};
StationSearch.prototype.setLimit = function(limit) {
  this.limit=limit;
};
StationSearch.priorityConfig={
  CODE_EXACT_MATCHED:10,
  CODE_STARTS_WITH:9,
  CODE_CONTAINS:8,
  NAME_EXACT_MATCH:7,
  NAME_STARTS_WITH:6,
  NAME_CONTAINS:5
} ;
StationSearch.prototype.searchStations= function (keyword){
  var result=[];
  var unSortedResult=[];
  keyword=keyword.trim();
  keyword=keyword.toLowerCase();
  if(keyword.length<3){
    return result;
  }

  for(var i=0;i<this.data.length;i++){
    var item=undefined;
    var stationCode=this.data[i]['code'].toLowerCase();
    var stationName=this.data[i]['name'].toLowerCase();
    if(stationCode===keyword){
      item=this.getItem_(this.data[i],StationSearch.priorityConfig.CODE_EXACT_MATCHED);
    }
    else if(stationCode.startsWith(keyword)){
      item=this.getItem_(this.data[i],StationSearch.priorityConfig.CODE_STARTS_WITH);
    }
    else if(stationCode.indexOf(keyword)!==-1){
      item=this.getItem_(this.data[i],StationSearch.priorityConfig.CODE_CONTAINS);
    }
    else if(stationName===keyword){
      item=this.getItem_(this.data[i],StationSearch.priorityConfig.NAME_EXACT_MATCH);
    }
    else if(stationName.startsWith(keyword)){
      item=this.getItem_(this.data[i],StationSearch.priorityConfig.NAME_STARTS_WITH);
    }
    else if(stationName.indexOf(keyword)!==-1){
      item=this.getItem_(this.data[i],StationSearch.priorityConfig.NAME_CONTAINS);
    }
    if(item!==undefined){
      unSortedResult.push(item);
    }
  }
  var sortedResults=unSortedResult.sort(function(a,b){
    if(a.order===b.order){
      return a['data']['name'].length > b['data']['name'].length ?1:-1;
    }
    return a.order < b.order ?1:-1;
  });
  var maxIndex=sortedResults.length<this.limit?sortedResults.length:this.limit;
  for(var i=0;i<maxIndex;i< i++){
    result.push(sortedResults[i]['data'])
  }
  return result;

};

StationSearch.prototype.getItem_=function(data,priority){
  return {
    'data':data,
    'order':priority
  };
};

