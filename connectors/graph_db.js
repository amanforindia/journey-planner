var url = require('url');
var neo4j = require('neo4j');

var properties = require('../config/neo4j.js');

var transaction_url_obj = {
    protocol: properties.NEO4J_PROTOCOL,
    hostname: properties.NEO4J_HOST,
    port: properties.NEO4J_PORT
};
var db_instance = null;

var GraphDB = function() {};

GraphDB.getInstance = function() {
    if (db_instance !== null) {
        return db_instance;
    }
    db_instance = new neo4j.GraphDatabase({
        url: url.format(transaction_url_obj),
        auth: {
            username: properties.NEO4J_USERNAME,
            password: properties.NEO4J_PASSWORD
        }
    });
    return db_instance;
};

module.exports = GraphDB;