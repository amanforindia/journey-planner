var cassandra = require('cassandra-driver');
var properties = require('../config/cassandra.js');

// Private shared variable so only 1 copy of client is kept
var client = null;
var db_instance = null;

var CacheDB = function() {
    this.getConnection = function() {
        if (client !== null) {
            return client;
        }
        return new cassandra.Client({
            contactPoints: [properties.CASSANDRA_HOST],
            keyspace: properties.CASSANDRA_KEYSPACE
        });
    }
};

CacheDB.getInstance = function() {
    if (db_instance !== null) {
        return db_instance.getConnection();
    }
    db_instance = new CacheDB();
    return db_instance.getConnection();
};

module.exports = CacheDB;