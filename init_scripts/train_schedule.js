var fs = require('fs');
var csv_loader = require('fast-csv');
var xpath = require('xpath');
var htmlparser = require('htmlparser2');
var request = require('request');
var json2csv = require('json2csv');

var stream = fs.createReadStream('../static_data/unique_train_number.csv');
var scheduleList = [];

var csv_stream = csv_loader({headers: true}).on('data', function(data) {
    request({
        uri: 'http://www.trainspnrstatus.com/runningstatus/' + data.train_no,
        method: 'GET'
    }, function(error, response, body) {
        var ifTableTag = false;
        var dataOfOneTrain = [];

        var parser = new htmlparser.Parser({
            onopentag: function(name, attribs) {
                if (name === 'table' && attribs.class === 'table table-striped table-bordered tablefont') {
                    ifTableTag = true;
                }
            },
            ontext: function(text) {
                if (ifTableTag && text !== 'undefined') {
                    dataOfOneTrain.push(text);
                }
            },
            onclosetag: function(tagname) {
                if (tagname === 'table') {
                    ifTableTag = false;
                }
            }
        }, {decodeEntities: true});

        parser.write(body);
        parser.end();

        var ifStarted = false;
        var matchedList;

        //scheduleList[data.train_no] = [];
        //scheduleList.push({});
        for (var i = 0; i < dataOfOneTrain.length; i++) {
            matchedList = dataOfOneTrain[i].match('([A-Z ]*)[ ]*[(]([A-Z]*)[)]');
            if (matchedList && matchedList.length !== 0) {
                scheduleList.push({});
                scheduleList[scheduleList.length - 1]['Station Name'] =
                    matchedList[1].trim();
                scheduleList[scheduleList.length - 1]['Station Code'] =
                    matchedList[2].trim();
                scheduleList[scheduleList.length - 1]['Train Number'] =
                    data.train_no;
                //scheduleList[data.train_no].push({});
                //scheduleList[data.train_no][scheduleList[data.train_no].length - 1]['Station Name'] =
                //    matchedList[1].trim();
                //scheduleList[data.train_no][scheduleList[data.train_no].length - 1]['Station Code'] =
                //    matchedList[2].trim();
                ifStarted = true;
            }
            matchedList = dataOfOneTrain[i].match('([A-Z][a-z]{2,})[ ]*([A-Z]{0,1}[a-z]*)');
            if (ifStarted && matchedList && matchedList.length !== 0) {
                scheduleList[scheduleList.length - 1]['Running Schedule'] =
                    matchedList[0].trim();
                //scheduleList[data.train_no][scheduleList[data.train_no].length - 1]['Running Schedule'] =
                //    matchedList[0].trim();
            }
        }
        //console.log(scheduleList);
        var fields = ['Train Number', 'Station Name', 'Station Code', 'Running Schedule'];
        json2csv({ data: scheduleList, fields: fields }, function(err, csv) {
            if (err) console.log(err);
            fs.writeFile('file.csv', csv, function(err) {
                if (err) throw err;
                console.log('file saved');
            });
        });
    });
}).on('end', function() {
    console.log('done');
});

stream.pipe(csv_stream);
