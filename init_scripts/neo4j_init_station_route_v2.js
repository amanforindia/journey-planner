/**
 * This version of data loader for neo4j uses the new format of data as in full_data_v2.csv and all the data are
 * expected to be extracted from this single file
 * @type {*|exports|module.exports}
 */
var csv_loader = require('fast-csv');
var fs = require('fs');
var trim = require('trim');
var HashMap = require('hashmap');
var extend = require('util')._extend;
var logger = require('winston');

var GraphDb = require('../connectors/graph_db.js');
var QueryLib = require('../util/cypher_statements.js');
var TimeSlot = require('../util/time-slot.js');

var _COMMIT_SIZE = 20000;

logger.add(logger.transports.File, { filename: 'logger.log' });
logger.remove(logger.transports.Console);

var GraphInitializer = function(train_schedule_file_path, data_file_path) {
  this._schedule_stream = fs.createReadStream(train_schedule_file_path);
  this._data_stream = fs.createReadStream(data_file_path);
  this._train_cache = new HashMap();
  this._schedule_cache = new HashMap();
  this._day_data_cache = new HashMap();

  GraphDb.getInstance().cypher({
    query: QueryLib.NEO4J.CREATE_CONSTRAINT_ON_STATION
  }, function(err, result) {
    if (!err) {
      console.log("Successfully added unique constraints on graph.");
    }
  });
};

GraphInitializer.prototype.startProcess = function() {
  var initFunc = this._loadScheduleData.bind(this, this._loadTrainData.bind(this, this._buildQueries.bind(this)));
  initFunc();
};

GraphInitializer.prototype._loadScheduleData = function(callback) {
  var self = this;
  var schedule_csv_stream = csv_loader({headers: true}).on('data', function(data) {
    self._schedule_cache.set([data.train_code, data.station_code], data.schedule_array.split(' '));
  }).on('end', function() {
    console.log('Completed loading schedule information.');
    callback();
  });

  this._schedule_stream.pipe(schedule_csv_stream);
};

/**
 * @param {function} callback This function will generate the queries
 * @private
 */
GraphInitializer.prototype._loadTrainData = function(callback) {
  console.log('Starting reading train and station data.');
  var self = this;
  var data_csv_stream = csv_loader({headers: true}).on('data', function(data) {
    self._train_cache.set([data.train_code, data.order], {
      train_code: data.train_code,
      order: data.order,
      station_code: data.station_code,
      station_name: data.station_name,
      arrival_time: data.arrival_time,
      departure_time: data.departure_time,
      distance: data.distance,
      source_station_code: data.source_station_code,
      destination_station_code: data.destination_station_code
    });
    self._day_data_cache.set([data.train_code, data.station_code], parseInt(data.day));
  }).on('end', function() {
    console.log('Completed loading data.');
    callback();
  });

  this._data_stream.pipe(data_csv_stream);
};

GraphInitializer.prototype._buildQueries = function() {
  console.log('Start building queries.');
  console.time('Time taken for building queries: ');
  var queries = [];
  var self = this;
  self._train_cache.forEach(function(value) {
    var train_code = value.train_code;
    var order = value.order;
    var iter_order = parseInt(order) - 1;
    while (iter_order > 0) {
      var start_node = self._train_cache.get([train_code, iter_order.toString()]);
      var end_node = value;
      if (!self._schedule_cache.get([train_code, trim(start_node.station_code)])) {
        logger.info('Failed for the train with train_code ' + train_code + '. No schedule found.');
        iter_order = iter_order - 1;
        continue;
      }
      if (!self._day_data_cache.get([trim(start_node.train_code), trim(end_node.station_code)])) {
        logger.info('Failed to get day schedule for train_code ' + train_code + ' and station_code' + trim(end_node.station_code));
        iter_order = iter_order -  1;
        continue;
      }
      try {
        queries.push({
          query: QueryLib.NEO4J.CREATE_STATION,
          params: {
            station: {
              code: trim(start_node.station_code),
              name: trim(start_node.station_name)
            }
          },
          lean: true
        });
        queries.push({
          query: QueryLib.NEO4J.CREATE_STATION,
          params: {
            station: {
              code: trim(end_node.station_code),
              name: trim(start_node.station_name)
            }
          },
          lean: true
        });
        self._schedule_cache.get([train_code, trim(start_node.station_code)]).forEach(function (value) {
          queries.push({
            query: QueryLib.NEO4J.CREATE_STATION_DATETIME_RELATION,
            params: {
              start_station: {
                code: trim(start_node.station_code)
              },
              end_station: {
                code: trim(end_node.station_code)
              },
              train: {
                code: trim(start_node.train_code),
                order: trim(end_node.order),
                distance: parseInt(end_node.distance) - parseInt(start_node.distance),
                departure_day: self._day_data_cache.get([trim(start_node.train_code), trim(start_node.station_code)]),
                arrival_day: self._day_data_cache.get([trim(start_node.train_code), trim(end_node.station_code)]),
                destination_arrival_time: TimeSlot.formatFromString(end_node.arrival_time),
                source_departure_time: TimeSlot.formatFromString(start_node.departure_time)
              },
              day_data: {
                station: trim(start_node.station_code),
                day: parseInt(value)
              },
              slotted_time: extend(TimeSlot.parse(start_node.departure_time).getTimeSlot(), {
                train_code: train_code,
                station: trim(start_node.station_code)
              })
            },
            lean: true
          });
        });
        iter_order = iter_order - 1;
      } catch (err) {
        console.log('====================================================================================');
        console.log('Unexpected error happened while loading data, please check the data for any issues')
        console.log('====================================================================================');
        console.log('Train Code: ' + train_code);
        console.log('Order: ' + order);
        console.log('Iter Order: ' + iter_order);
        throw err;
      }
    }
  });
  console.timeEnd('Time taken for building queries: ');
  console.log('Generating graph with help of ' + queries.length + ' queries, with individual commit size: ' + _COMMIT_SIZE);
  logger.info('Generating graph with help of ' + queries.length + ' queries, with individual commit size: ' + _COMMIT_SIZE);

  // Free all other cached resources
  this._train_cache.clear();
  this._schedule_cache.clear();
  this._train_cache = null;
  this._schedule_cache = null;

  // Flush the query array by generating graph for the last section
  this._generateGraph(queries, 0);
};

/**
 * Will create the graph connectivity
 * @param {Array.<Object>} queries all queries to execute in batch for construction of graph
 * @param {number} query_pointer
 */
GraphInitializer.prototype._generateGraph = function(queries, query_pointer) {
  console.time('Time taken for generating complete graph: ');
  console.time('Batch query execution time: ');
  var callback = function(err, batchResults) {
    var queries_executed = query_pointer + _COMMIT_SIZE;
    if (err) {
      console.log(err);
      logger.error('Failed while generating graph at index: ' + query_pointer + ', resuming from: ' + queries_executed);
    } else {
      console.timeEnd('Batch query execution time: ');
      console.log('Queries executed: ' + queries_executed);
      logger.info('Queries executed: ' + queries_executed);
      console.log('Percentage finished: ' + (queries_executed * 100) / queries.length);
      if (query_pointer < queries.length - _COMMIT_SIZE) {
        this._generateGraph(queries, queries_executed);
      } else {
        logger.info('Completed creating Graph');
        console.timeEnd('Time taken for generating complete graph: ');
      }
    }
  }.bind(this);
  GraphDb.getInstance().cypher({
    queries: queries.slice(query_pointer, query_pointer + _COMMIT_SIZE)
  }, callback);
};

module.exports = GraphInitializer;

var graphInit = new GraphInitializer('../static_data/data_for_train_schedule_final.csv', '../static_data/v2/part_data_v2_4.csv');
graphInit.startProcess();
