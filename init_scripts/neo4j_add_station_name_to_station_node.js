/**
 * The data initialized to one of the versions didn't initialize with station name. This file adds the station_name to each station node
 */
var csv_loader = require('fast-csv');
var fs = require('fs');
var trim = require('trim');

var GraphDb = require('../connectors/graph_db.js');

var ADD_STATION_NAME_TO_NODE = "MATCH (stn:Station) WHERE stn.code={station}.code SET stn.name={station}.name";

var train_code_name_map = fs.createReadStream('../static_data/station_code_to_station_name_map.csv');
var queriesArray = [];
var target_map_stream = csv_loader({headers: true}).on('data', function(data) {
  queriesArray.push({
    query: ADD_STATION_NAME_TO_NODE,
    params: {
      station: {
        code: trim(data.station_code),
        name: trim(data.station_name)
      }
    }
  });
}).on('end', function() {
  console.log('Adding station name to nodes');
  GraphDb.getInstance().cypher({
    queries: queriesArray
  }, function() {
    console.log('Finished');
  });
});

train_code_name_map.pipe(target_map_stream);
