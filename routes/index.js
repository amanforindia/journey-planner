var express = require('express');
var railService =require('../service/railway-api-service');
var router = express.Router();

router.get('/api/getJourneys', function(req, res) {
    railService.getAvailability('12615','GWL','MTJ','04-05-2016','3A','GN',function(response){
        res.json(response);
    });
});

router.get('/api/getStationCode', function(req, res) {
    railService.getStationCode('Bikaner Jn',function(response){
        res.json(response);
    });
});
module.exports = router;
