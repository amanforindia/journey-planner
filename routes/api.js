/**
 * Created by aman on 5/3/16.
 */
var express = require('express');
var trainRoutesService = require('../service/train_routes');
var Helpers = require('../util/Helper');
var trainFareService = require('../service/train-fare-service');
var railwayApiService = require('../service/railway-api-service');
var router = express.Router();
var async = require('async');

/**
 * body params: {
 *  startTime: {long, millis since 1970}
 *  src: {String}
 *  dest: {String}
 *  quota: {String, def: GN} (ref: http://www.indianrail.gov.in/quota_Code.html)
 *  class: {String, def: SL} (ref: http://www.indianrail.gov.in/class_Code.html)
 *  maxChanges: {int, def: 2}
 *  minTimeBetweenTrains: {float (number of minutes), def: 30}
 *  maxTimeBetweenTrains: {float (number of minutes), def: 600}
 *  costLimit: {int (number of Rs), def: -1 (no limit)}
 *  timeLimit: {float (number of days), def: -1 (no limit)}
 * }
 * @return Array<Array<{
 *  code: {int}
 *  name: {int}
 *  startTime: {long, millis since 1970}
 *  endTime: {long, millis since 1970}
 *  srcCode: {String}
 *  srcName: {String}
 *  destCode: {String}
 *  destName: {String}
 *  cachedAvailability: { {code: String, avail: int, time: long} } (not present if not cached)
 *  class: {String} (maybe useful in case default class not present on train)
 *  cost: {float (Rs)}
 * }>>
 */
router.post('/getTrains', function (req, res) {

  var body = req.body;
  var maxChanges = parseInt(body.maxChanges) || 1;
  if (body.src) {
    // console.log(body);
    // TODO: How to deal with the maxchanges array (how should the client manage it)
    trainRoutesService.getRoutes(body.src, body.dest, body.startTime, [1, 2], function (err, results) {
      var trainCodes = {};
      // var allTrains = {};
      var routes = results.map(function (route) {
        var keys = Object.keys(route).sort();
        var trains = [];
        for (var i = 1; i < keys.length - 2; i += 3) {
          var train = {};
          train.trainNo = route[keys[i + 1]].code;
          train.name = route[keys[i + 1]].name;
          train.startTime = Helpers.getTimeFromDateAndHours(body.startTime,
            route[keys[i + 1]].source_departure_time, route[keys[i]].day, null, null);
          train.endTime = Helpers.getTimeFromDateAndHours(body.startTime, route[keys[i + 1]].destination_arrival_time,
            route[keys[i]].day, route[keys[i + 1]].departure_day, route[keys[i + 1]].arrival_day);
          train.srcCode = route[keys[i - 1]].code;
          train.srcName = route[keys[i - 1]].name;
          train.destCode = route[keys[i + 2]].code;
          train.destName = route[keys[i + 2]].name;
          train.distance = route[keys[i + 1]].distance;
          var requestObj={};
          requestObj[train.trainNo]=train;
          trainFareService.getFare(requestObj );
          trains.push(train);
        }
        return trains;
      });
      res.json(routes);
    });
  }
});


/**
 * Hard refresh availabilities for given trains and update cache db.
 * body params: Array<Train: {
 *  trainCode: {int}
 *  src: {String}
 *  dest: {String}
 *  startTime: {long}
 *  quota: {String}
 *  class: {String}
 * }>
 * @return Array<{
 *  trainCode: {int}
 *  src: {String}
 *  dest: {String}
 *  startTime: {long}
 *  quota: {String}
 *  class: {String}
 *  Array<availability: {code: String, avail: int, time: long}>
 * }
 */
router.post('/getAvailabilities', function (req, res) {
  var trains = req.body.trains;
  async.map(trains, function(train, cb) {
    railwayApiService.getAvailability(train.trainCode, train.src, train.dest, train.startTime,
        train.class, train.quota, cb);
  }, function(err, results) {
    console.log(results);
    res.json(results);
  });
});

/**
 * Try jugaad.
 * body params: Train
 * @return Array<Array<Train: {
 *  trainCode: {int}
 *  src: {String}
 *  dest: {String}
 *  startTime: {long}
 *  endTime: {long}
 *  quota: {String}
 *  class: {String}
 *  cost: {float (Rs)}
 * }
 */
router.post('/getAlternateRoutes', function (req, res) {

});

module.exports = router;
