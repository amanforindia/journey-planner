var properties = {
    CASSANDRA_HOST: 'ec2-52-221-230-94.ap-southeast-1.compute.amazonaws.com',
    CASSANDRA_PORT: '9042',
    CASSANDRA_KEYSPACE: 'cache_strategy'
};

module.exports = properties;
