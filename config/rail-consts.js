var consts = {};
consts.properties = {
    TRAIN_NUMBER :'train',
    SOURCE : 'source',
    DESTINATION : 'dest',
    DATE: 'date',
    STATION_NAME: 'station',
    CLASS_CODE: 'class',
    QUOTA:'quota',
    API_KEY:'apikey',
    API_KEY_VALUE:'gqrao1598'
};
consts.apitype = {
    CHECK_SEAT:'check_seat',
    NAME_TO_CODE:'name_to_code'
};

module.exports = consts;